package org.wangjie.analysisRSSI;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName org.wangjie.analysisRSSI.AnalysisRSSIApplication
 * @Description //TODO
 * @Author Han ChengYi
 * @Date 2020/12/14 18:20
 * @Version 1.0
 **/
@SpringBootApplication(scanBasePackages = {"org.wangjie.analysisRSSI"})
@MapperScan("org.wangjie.analysisRSSI.mapper")
public class AnalysisRSSIApplication {
    public static void main(String[] args) {
        SpringApplication.run(AnalysisRSSIApplication.class,args);
    }
}
