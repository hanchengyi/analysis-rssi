package org.wangjie.analysisRSSI.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.wangjie.analysisRSSI.model.entity.SnifferdataEntity;

import java.util.List;

/**
 * (Snifferdata)表数据库访问层
 *
 * @author Cheney Han
 * @since 2021-03-05 11:59:08
 */
@Repository
public interface SnifferdataMapper extends BaseMapper<SnifferdataEntity> {
    /**
     * 按照 srcAddress 查询最后一条记录值
     * @param srcaddress
     * @return
     */
    SnifferdataEntity selectLastRecordBySrcAddress(String srcaddress);

    /**
     * 查询所有记录
     * @return
     */
    int countAllSnifferdataEntities();

    /**
     * 查询所有的DestPan为 0xFFFF
     */
    int countAllSnifferByDestPan();

    /**
     *  分页查询
     * @return
     */
    List<SnifferdataEntity> selectByPage(int pageNumber, int pageSize);

    /**
     * 选择THL记录
     * @return
     */
    List<SnifferdataEntity> selectTHL(String destAddress);
    List<SnifferdataEntity> selectFromOrigin();

    /**
     * 统计源地址的个数
     * @return
     */
    int countSrcAddress();

//    List<SnifferdataEntity> select

    /**
     * 查询数据
     * @return
     */
    List<SnifferdataEntity> selectAll();

    /**
     * 按照源地址查询记录
     * @param srcAddress
     * @return
     */
    List<SnifferdataEntity> selectByNode(String srcAddress);

    /**
     * 按照副本筛选
     * @return
     */
    List<SnifferdataEntity> selectBynumberOfCopies(String srcAddress);

    /**
     * 路径扰动
     * @param srcAddress
     * @return
     */
    List<SnifferdataEntity> selectBypathTurbulence(String srcAddress);

    /**
     * 按流量查询
     * @param srcAddress
     * @return
     */
    List<SnifferdataEntity> selectByDataSize(String srcAddress);

    List<SnifferdataEntity> selectByFlow(String srcAddress);

    /**
     * 按照副本查询
     * @param srcAddress
     * @return
     */
    List<SnifferdataEntity> selectByumberOfCopies(String srcAddress);

    /**
     * 广播包数据
     * @return
     */
    List<SnifferdataEntity> selectByumberOfBroadCast(String srcAddress);
    List<SnifferdataEntity> selectByumberOfBroadCastInSystem();

    /**
     * 系统流量
     * @return
     */
    List<SnifferdataEntity> selectByFlowInSystem();
    List<SnifferdataEntity> selectByDataSizeInSystem();

    /**
     * 系统路径扰动
     * @return
     */
    List<SnifferdataEntity>  selectBypathTurbulenceInSystem();

    /**
     * 系统副本数量
     * @return
     */
    List<SnifferdataEntity> selectBynumberOfCopiesInSystem();
}