package org.wangjie.analysisRSSI.model.data;

import lombok.Data;

/**
 * @ClassName MaxAndMean
 * @Description //TODO
 * @Author Han ChengYi
 * @Date 2021/3/6 22:20
 * @Version 1.0
 **/
@Data
public class MaxAndMean {
    private int max = Integer.MIN_VALUE;
    private int sum = 0;
    private int size = 0;
    private float meanValue = 0.0f;
}
