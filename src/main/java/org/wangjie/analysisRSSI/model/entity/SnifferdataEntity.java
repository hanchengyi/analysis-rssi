package org.wangjie.analysisRSSI.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * (Snifferdata)实体类
 *
 * @author Cheney Han
 * @since 2021-03-05 11:59:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("snifferdata")
@Accessors(chain = true)
public class SnifferdataEntity implements Serializable {
    private static final long serialVersionUID = -72059096969281199L;

    /**
    * 在PSD文件中的序列
    */
    @TableId(type = IdType.ASSIGN_ID, value = "seqnuminpsdfile")
    private Integer seqnuminpsdfile;

    /**
    * 长度
    */
    @TableField("length")
    private Integer length;

    /**
    * FCF
    */
    @TableField("fcf")
    private String fcf;

    /**
    * dsn
    */
    @TableField("dsn")
    private Integer dsn;

    /**
    * destpan
    */
    @TableField("destpan")
    private String destpan;

    /**
    * destaddress
    */
    @TableField("destaddress")
    private String destaddress;

    /**
    * srcaddress
    */
    @TableField("srcaddress")
    private String srcaddress;

    /**
    * payload
    */
    @TableField("payload")
    private String payload;

    /**
    * fcs
    */
    @TableField("fcs")
    private String fcs;

}