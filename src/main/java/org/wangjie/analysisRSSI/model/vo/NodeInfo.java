package org.wangjie.analysisRSSI.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @ClassName NodeInfo
 * @Description //TODO
 * @Author Han ChengYi
 * @Date 2021/3/14 20:58
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class NodeInfo {
    /**
     * 节点Id
     */
    private String nodeId;

    /**
     * 包据量
     */
    private String numberOfPackets;

    /**
     * 丢包数
     */
    private String packetLossCount;

    /**
     * 丢包量
     */
    private String packetLossRate;

    /**
     * ETX
     */
    private String ETX;

    /**
     * RSSI 均值
     */
    private String meanRSSI;

    /**
     * LQI均值
     */
    private String meanLQI;

    /**
     * 最大跳数
     */
    private String maxHop;

    /**
     * 均值跳数
     */
    private String meanHop;

    /**
     * 包交付率
     */
    private String packageDeliveryRate;

    /**
     * 路径扰动
     */
    private String pathTurbulence;

    /**
     * 数据总量
     */
    private String dataSize;

    /**
     * 广播包
     */
    private String broadcastPackage;

    /**
     * 广播包比率
     */
    private String broadcastPackageRate;

    /**
     * 流量
     */
    private String flow;

    /**
     * 副本数量
     */
    private String numberOfCopies;

    /**
     * 副本比例
     */
    private String numberOfCopiesRate;
}
