package org.wangjie.analysisRSSI.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @ClassName SinfferDataVO
 * @Description //TODO
 * @Author Han ChengYi
 * @Date 2021/3/5 14:15
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SinfferDataVO {
    /**
     * 在PSD文件中的序列
     */
    private Integer seqnuminpsdfile;

    /**
     * 长度
     */
    private Integer length;

    /**
     * FCF
     */
    private String type;

    /**
     * dsn
     */
    private Integer dsn;

    /**
     * destpan
     */
    private String destpan;

    /**
     * destaddress
     */
    private String destaddress;

    /**
     * srcaddress
     */
    private String srcaddress;

    /**
     * payload
     */
    private String payload;

    /**
     * fcs
     */
    private String RSSI;

    /**
     * LQI质量
     */
    private String LQI;

    /**
     * CRC校验
     */
    private String CRC;
}
