package org.wangjie.analysisRSSI.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @ClassName SystemInfo
 * @Description //TODO
 * @Author Han ChengYi
 * @Date 2021/3/15 15:22
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SystemInfo {
    /**
     * 数据总量
     */
    private String nodeCount;
    /**
     * ETX均值
     */
    private String meanETX;
    /**
     * 丢包量
     */
    private String packetLossCount;
    /**
     * 丢包率
     */
    private String packetLossRate;
    /**
     * 包交付率
     */
    private String packageDeliveryRate;
    /**
     * RSSI均值
     */
    private String meanRSSI;
    /**
     * LQI均值
     */
    private String meanLQI;
    /**
     * 网络最大跳数
     */
    private String systemMaxHop;
    /**
     * 网络平均跳数
     */
    private String systemMeanHop;
    /**
     * data总量
     */
    private String dataSize;
    /**
     * 控制开销数量(广播包)
     */
    private String broadcastPackage;

    /**
     * 广播包比率
     */
    private String broadcastPackageRate;

    /**
     * 平均路径扰动
     */
    private String meanPathTurbulence;
    /**
     * 副本数据
     */
    private String numberOfCopies;

    /**
     * 副本数据比例
     */
    private String numberOfCopiesRate;
//    /**
//     * 数据包数量
//     */
//    private String numberOfPackets;
//
    /**
     * ACK 响应
     */
//    private String numberOfACK;
    /**
     * 流量
     */
    private String flow;
}
