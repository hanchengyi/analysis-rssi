package org.wangjie.analysisRSSI.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.wangjie.analysisRSSI.model.entity.SnifferdataEntity;
import org.wangjie.analysisRSSI.model.vo.NodeInfo;
import org.wangjie.analysisRSSI.model.vo.SinfferDataVO;
import org.wangjie.analysisRSSI.model.vo.SystemInfo;

import java.util.List;

/**
 * (Snifferdata)表服务接口
 *
 * @author Cheney Han
 * @since 2021-03-05 11:59:08
 */
public interface SnifferdataService extends IService<SnifferdataEntity> {
    /**
     * 查询 DSN 的个数是多少个 ？
     * @return
     */
    int selectNumberBy255();

    /**
     * 查询最后一个 srcAddress 的 DSN 值是多少 ？
     * @param srcAddress
     * @return
     */
    SnifferdataEntity selectLastRecordBySrcAddress(String srcAddress);

    /**
     * 查询所有的数据条数
     * @return Sniffer Data 中的所有条目数据
     */
    int countAllSnifferdataEntities();

    /**
     * 按照DestPan == 0xFFFF的个数来寻找个数
     * @return
     */
    int countAllSnifferByDestPan();

    /**
     * 分页查询
     * @param pageNumber 分页号码
     * @param pageSize 分页大小
     * @return
     */
    List<SinfferDataVO> selectByPage(int pageNumber, int pageSize);

    /**
     *
     * @param destAddress
     * @return
     */
    List<SnifferdataEntity> selectTHL(String destAddress);

    /**
     * 按照节点查询
     * @param srcAddress
     * @return
     */
    List<SnifferdataEntity> selectByNode(String srcAddress);

    /**
     * 求取 网络中 ETX 均值
     * @return
     */
    int meanETX();

    /**
     * 计算网络系统总指标
     * @return
     */
    SystemInfo calcSysetmIndicator(int srcaddress);

    /**
     * 计算节点信息
     */
    NodeInfo calcNodeInfo(String srcAddress);
}