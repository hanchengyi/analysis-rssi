package org.wangjie.analysisRSSI.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wangjie.analysisRSSI.mapper.SnifferdataMapper;
import org.wangjie.analysisRSSI.model.data.MaxAndMean;
import org.wangjie.analysisRSSI.model.entity.SnifferdataEntity;
import org.wangjie.analysisRSSI.model.vo.NodeInfo;
import org.wangjie.analysisRSSI.model.vo.SinfferDataVO;
import org.wangjie.analysisRSSI.model.vo.SystemInfo;
import org.wangjie.analysisRSSI.service.SnifferdataService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * (Snifferdata)表服务实现类
 *
 * @author Cheney Han
 * @since 2021-03-05 11:59:08
 */
@Service("snifferdataService")
public class SnifferdataServiceImpl extends ServiceImpl<SnifferdataMapper, SnifferdataEntity> implements SnifferdataService {

    @Autowired
    private SnifferdataMapper snifferdataMapper;

    @Override
    public int selectNumberBy255() {
        QueryWrapper<SnifferdataEntity> snifferdataEntityQueryWrapper = new QueryWrapper<>();
        snifferdataEntityQueryWrapper.ne("fcf","0x0002");
        List<SnifferdataEntity> snifferdataEntities = snifferdataMapper.selectList(snifferdataEntityQueryWrapper);
        int numOf255 = snifferdataEntities.stream().filter(e->e.getDsn() == 255).collect(Collectors.toList()).size();
        int numOf254 = snifferdataEntities.stream().filter(e->e.getDsn() == 254).collect(Collectors.toList()).size();
        int numOf253 = snifferdataEntities.stream().filter(e->e.getDsn() == 253).collect(Collectors.toList()).size();
        int result = Math.max(Math.max(numOf255,numOf254),numOf253);
        return result;
    }

    @Override
    public SnifferdataEntity selectLastRecordBySrcAddress(String srcAddress) {
        return snifferdataMapper.selectLastRecordBySrcAddress(srcAddress);
    }

    @Override
    public int countAllSnifferdataEntities() {
        return snifferdataMapper.countAllSnifferdataEntities();
    }

    @Override
    public int countAllSnifferByDestPan() {
        return snifferdataMapper.countAllSnifferByDestPan();
    }

    @Override
    public List<SinfferDataVO> selectByPage(int pageNumber, int pageSize) {
        ArrayList<SinfferDataVO> sinfferDataVOS = new ArrayList<>();
        List<SnifferdataEntity> snifferdataEntities = snifferdataMapper.selectByPage((pageNumber) * pageSize, pageSize);
        for(SnifferdataEntity snifferdataEntity : snifferdataEntities){
            SinfferDataVO sinfferDataVO = new SinfferDataVO();
            sinfferDataVO.setSeqnuminpsdfile(snifferdataEntity.getSeqnuminpsdfile());
            sinfferDataVO.setLength(snifferdataEntity.getLength());
            String fcf = snifferdataEntity.getFcf();
            String type = "";
            switch (fcf){
                case "0x8841":
                    type = "DATA";
                    break;
                case "0x8861":
                    type = "DATA";
                    break;
                case "0x0002":
                    type = "ACK";
                    break;
                default:
                    type = "ERROR";
                    break;
            }
            sinfferDataVO.setType(type);
            sinfferDataVO.setDsn(snifferdataEntity.getDsn());
            sinfferDataVO.setDestpan(snifferdataEntity.getDestpan());
            sinfferDataVO.setDestaddress(snifferdataEntity.getDestaddress());
            sinfferDataVO.setSrcaddress(snifferdataEntity.getSrcaddress());
            sinfferDataVO.setPayload(snifferdataEntity.getPayload());
            int fcs = Integer.parseInt(snifferdataEntity.getFcs().substring(4), 16);
            int RSSI = fcs > 128 ? fcs - 256 - 73 : fcs - 73;
            sinfferDataVO.setRSSI(Integer.toString(RSSI));
            int halfFcs = Integer.parseInt(snifferdataEntity.getFcs().substring(2, 4), 16);
            if( halfFcs >= 128){
                sinfferDataVO.setCRC("OK");
            }else{
                sinfferDataVO.setCRC("ERROR");
            };
            sinfferDataVO.setLQI(String.valueOf(halfFcs & 0x7F));
            sinfferDataVOS.add(sinfferDataVO);
        }
        return  sinfferDataVOS;
    }

    @Override
    public List<SnifferdataEntity> selectTHL(String destAddress) {
        return snifferdataMapper.selectTHL(destAddress);
    }

    @Override
    public List<SnifferdataEntity> selectByNode(String srcAddress) {
        return snifferdataMapper.selectByNode(srcAddress);
    }

    @Override
    public int meanETX() {
        return 0;
    }

    @Override
    public SystemInfo calcSysetmIndicator(int srcaddress) {
        SystemInfo systemInfo = new SystemInfo();
        List<SnifferdataEntity> snifferdataEntities = snifferdataMapper.selectAll();
        /**
         * 数据总量
         */
        systemInfo.setNodeCount(String.valueOf(snifferdataEntities.size()));

        /**
         * DataSize
         */
        List<SnifferdataEntity> snifferdataDataSize = this.snifferdataMapper.selectByDataSizeInSystem();
        snifferdataDataSize = snifferdataDataSize.stream()
                .filter(e -> {
                    int halfFcs = Integer.parseInt(e.getFcs().substring(2, 4), 16);
                    return halfFcs > 128;
                })
                .collect(Collectors.toList());

        int dataSize = snifferdataDataSize.size();
        systemInfo.setDataSize(String.valueOf(dataSize));

        /**
         * 丢包率
         *
         */
        int numberOf255 = selectNumberBy255();
//        int totalNumberOfReceive = countAllSnifferdataEntities();
        int totalNumberOfSend = numberOf255 * 256;

        String[] address = new String[] {"0x0001","0x0002","0x0003","0x0004","0x0005","0x0006", "0x0007","0x0008","0x0009","0x000A","0x000B","0x000C","0x000D","0x000E","0x000F"};
        for(int i = 1; i < srcaddress; ++i){
            int dsn = selectLastRecordBySrcAddress(address[i]).getDsn();
            totalNumberOfSend += dsn;
        }
        System.out.println("DataSize" + systemInfo.getDataSize());
        int packetloss = totalNumberOfSend - Integer.parseInt(systemInfo.getDataSize());
        systemInfo.setPacketLossCount(String.valueOf(packetloss));
        float packetlossRate = (float)Integer.parseInt(systemInfo.getDataSize()) / totalNumberOfSend;
        systemInfo.setPacketLossRate(String.format("%.2f",packetlossRate));
        float ETX = 1/(packetlossRate);
        systemInfo.setMeanETX(String.format("%.2f",ETX));
        /**
         * RSSI
         * LQI
         */
        int sumRSSI = 0;
        int sumLQI = 0;
        for(SnifferdataEntity snifferdataEntity : snifferdataEntities) {
            int fcs = Integer.parseInt(snifferdataEntity.getFcs().substring(4), 16);
            int RSSI = fcs > 128 ? fcs - 256 - 73 : fcs - 73;
            sumRSSI += RSSI;
            int halfFcs = Integer.parseInt(snifferdataEntity.getFcs().substring(2, 4), 16);
            sumLQI += (halfFcs & 0x7F);
        }
        int size = snifferdataEntities.size();
        systemInfo.setMeanRSSI(String.format("%.1f",(float)sumRSSI/size));
        systemInfo.setMeanLQI(String.format("%.1f",(float)sumLQI/size));
        /**
         * 网络最大跳数
         * 网络平均跳数
         */
        List<SnifferdataEntity> snifferdataEntitiesTHL = selectTHL("0x0001");
        snifferdataEntitiesTHL = snifferdataEntitiesTHL
                .stream()
                .filter(e -> {
                    int halfFcs = Integer.parseInt(e.getFcs().substring(2, 4), 16);
                    return halfFcs > 128;
                })
                .collect(Collectors.toList());

        HashMap<String, MaxAndMean> stringIntegerHashMap = new HashMap<>();
        for (SnifferdataEntity snifferdataEntity : snifferdataEntitiesTHL) {
            String originAddress = snifferdataEntity.getPayload().substring(12, 16);
            if(stringIntegerHashMap.containsKey(originAddress)){
                int hopCount = Integer.parseInt(snifferdataEntity.getPayload().substring(6, 8), 16);
                MaxAndMean maxAndMean = stringIntegerHashMap.get(originAddress);
                maxAndMean.setMax(Math.max(hopCount + 1, maxAndMean.getMax()));
                maxAndMean.setSize(maxAndMean.getSize() + 1);
                maxAndMean.setSum(maxAndMean.getSum() + hopCount);
                maxAndMean.setMeanValue((float)maxAndMean.getSum()/(float)maxAndMean.getSize() + 1);
            }else{
                int hopCount = Integer.parseInt(snifferdataEntity.getPayload().substring(6, 8), 16);
                MaxAndMean maxAndMean = new MaxAndMean();
                maxAndMean.setMax(Math.max(hopCount + 1, maxAndMean.getMax()));
                maxAndMean.setSize(maxAndMean.getSize() + 1);
                maxAndMean.setSum(maxAndMean.getSum() + hopCount);
                maxAndMean.setMeanValue((float)maxAndMean.getSum()/(float)maxAndMean.getSize() + 1);
                stringIntegerHashMap.put(originAddress, maxAndMean);
            }
        }
        Set<String> keySet = stringIntegerHashMap.keySet();
        int maxHop = 0;
        int sumHop = 0;
        for (String s : keySet) {
            maxHop = Math.max(maxHop, stringIntegerHashMap.get(s).getMax());
            sumHop += stringIntegerHashMap.get(s).getMeanValue();
        }
        systemInfo.setSystemMaxHop(String.valueOf(maxHop));
        systemInfo.setSystemMeanHop(String.format("%.1f",(float)sumHop/keySet.size()));
        /**
         * 广播包
         */
        List<SnifferdataEntity> snifferdataByumberOfBroadCast = this.snifferdataMapper.selectByumberOfBroadCastInSystem();
        snifferdataByumberOfBroadCast = snifferdataByumberOfBroadCast.stream()
                .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                .collect(Collectors.toList());
        System.out.println("=============" + snifferdataByumberOfBroadCast.size());
        systemInfo.setBroadcastPackage(String.valueOf(snifferdataByumberOfBroadCast.size()));
        /**
         * dataSize
         * 流量
         * 广播包比例
         */

        systemInfo.setBroadcastPackageRate(String.format("%.2f",(float)snifferdataByumberOfBroadCast.size()/Integer.parseInt(systemInfo.getNodeCount())));

        List<SnifferdataEntity> snifferdataFlow = this.snifferdataMapper.selectByFlowInSystem()
                                                    .stream()
                                                    .filter(e -> {
                                                        int halfFcs = Integer.parseInt(e.getFcs().substring(2, 4), 16);
                                                        return halfFcs > 128;
                                                    })
                                                    .collect(Collectors.toList());

        int flow = 0;
        for (SnifferdataEntity snifferdataEntity : snifferdataFlow) {
            flow += snifferdataEntity.getLength();
        }
        systemInfo.setFlow(String.valueOf(flow));
        /**
         * 平均路径扰动
         */
        int pathTurb = 0;
        int pathTurbLen = 0;
        for(int j = 0; j < srcaddress; ++j){
            List<SnifferdataEntity> snifferdataEntitiesBypathTurbulence = this.snifferdataMapper.selectBypathTurbulence(address[j]);
            snifferdataEntitiesBypathTurbulence = snifferdataEntitiesBypathTurbulence.stream()
                    .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                    .collect(Collectors.toList());
            pathTurbLen += snifferdataEntitiesBypathTurbulence.size();
            for (int i = 0; i <= snifferdataEntitiesBypathTurbulence.size() - 2; ++i) {
                if(!snifferdataEntitiesBypathTurbulence.get(i).getDestaddress().equals(snifferdataEntitiesBypathTurbulence.get(i + 1).getDestaddress())){
                    pathTurb++;
                }
            }
        }
        systemInfo.setMeanPathTurbulence(String.format("%.2f",(float)pathTurb/(srcaddress - 1)));
        /**
         * 副本数量
         * 副本比率
         */
//        if(srcAddress == "0x0001"){
//            nodeInfo.setNumberOfCopies("0");
//            nodeInfo.setNumberOfCopiesRate("0.00");
//
//        }else{
            List<SnifferdataEntity> snifferdataBynumberOfCopies = this.snifferdataMapper.selectBynumberOfCopiesInSystem();
            snifferdataBynumberOfCopies = snifferdataBynumberOfCopies.stream()
                    .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                    .collect(Collectors.toList());
            HashSet<String> set2 = new HashSet<>();
            for (SnifferdataEntity snifferdataBynumberOfCopy : snifferdataBynumberOfCopies) {
                String originAddress = snifferdataBynumberOfCopy.getPayload().substring(12, 16);
//                if(srcAddress.substring(2).equals(originAddress)){
                    String payload = snifferdataBynumberOfCopy.getPayload();
                    set2.add(payload.substring(6,8) + payload.substring(12, 20));
//                }
            }
        systemInfo.setNumberOfCopies(String.valueOf(set2.size()));
        systemInfo.setNumberOfCopiesRate(String.format("%.2f",(float)set2.size()/Integer.parseInt(systemInfo.getNodeCount())));
//        }
        /**
         * 包到达率
         */
        int totalSize = 0;
        int totalCount = 0;
        List<SnifferdataEntity> snifferdataFromOrigin = this.snifferdataMapper.selectFromOrigin();
        System.out.println("snifferdataFromOrigin.size() = " + snifferdataFromOrigin.size());


        for(int m = 1; m < srcaddress; ++m){
            // fcs > ok
            ArrayList<SnifferdataEntity> collect = (ArrayList<SnifferdataEntity>) snifferdataFromOrigin
                    .stream()
                    .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                    .collect(Collectors.toList());
            // 筛分的

            for(int k = 0 ; k < collect.size(); ++k){
                if(!collect.get(k).getPayload().substring(12, 16).equals(address[m].substring(2))) {
                    collect.remove(k);
                    k--;
                }
            }

            String payload = collect.get(collect.size() - 1).getPayload();
            int realPackageCount = Integer.parseInt(payload.substring(payload.length() - 8, payload.length() - 4),16);
            totalCount += realPackageCount;
            collect = (ArrayList<SnifferdataEntity>) collect.stream()
                    .filter(e -> e.getDestaddress().equals("0x0001"))
                    .collect(Collectors.toList());
            Set<String> set = new TreeSet<>();
            for (SnifferdataEntity snifferdataEntity : collect) {
                String payload1 = snifferdataEntity.getPayload();
                set.add(payload1.substring(payload1.length() - 8,payload1.length() - 4));
            }
            totalSize += set.size();
            set.clear();
        }
        systemInfo.setPackageDeliveryRate(String.format("%.2f",(float)totalSize/totalCount));
        System.out.println(systemInfo);
        return systemInfo;
    }

    @Override
    public NodeInfo calcNodeInfo(String srcAddress) {
        NodeInfo nodeInfo = new NodeInfo();
        /**
         * nodeId
         */
        nodeInfo.setNodeId(srcAddress);
        /**
         * dataSize
         */
        List<SnifferdataEntity> snifferdataDataSize = this.snifferdataMapper.selectByDataSize(srcAddress);

        int dataSize = snifferdataDataSize.size();
        nodeInfo.setDataSize(String.valueOf(dataSize));
        /**
         * ETX
         * 包量
         * 丢包率
         * 丢包量
         */
        List<SnifferdataEntity> snifferdataEntities = snifferdataMapper.selectByNode(srcAddress);
        SnifferdataEntity lastSnifferdataEntity = snifferdataEntities.get(snifferdataEntities.size() - 1);
        int dsn255size = snifferdataEntities.stream().filter(e -> e.getDsn() == 255).collect(Collectors.toList()).size();
        int dsn254size = snifferdataEntities.stream().filter(e -> e.getDsn() == 254).collect(Collectors.toList()).size();
        int dsn253size = snifferdataEntities.stream().filter(e -> e.getDsn() == 253).collect(Collectors.toList()).size();
        int size = snifferdataEntities.size();
        int dataTotalCount = Math.max(Math.max(dsn255size,dsn254size),dsn253size) * 256 + lastSnifferdataEntity.getDsn();
        int packetLossCount = dataTotalCount - dataSize;
        nodeInfo.setNumberOfPackets(String.valueOf(size));
        nodeInfo.setPacketLossCount(String.valueOf(packetLossCount));
        nodeInfo.setPacketLossRate(String.format("%.2f",(float) dataSize/dataTotalCount));
        nodeInfo.setETX(String.format("%.2f",(float) dataTotalCount/dataSize));
        /**
         * 计算 RSSI 和 LQI
         */
        int sumRSSI = 0;
        int sumLQI = 0;
        for(SnifferdataEntity snifferdataEntity : snifferdataEntities) {
            int fcs = Integer.parseInt(snifferdataEntity.getFcs().substring(4), 16);
            int RSSI = fcs > 128 ? fcs - 256 - 73 : fcs - 73;
            sumRSSI += RSSI;
            int halfFcs = Integer.parseInt(snifferdataEntity.getFcs().substring(2, 4), 16);
            sumLQI += (halfFcs & 0x7F);
        }
        nodeInfo.setMeanRSSI(String.format("%.1f",(float)sumRSSI/size));
        nodeInfo.setMeanLQI(String.format("%.1f",(float)sumLQI/size));
        /**
         * 最大跳数 平均跳数
         */
        List<SnifferdataEntity> snifferdataEntitiesTHL = selectTHL("0x0001");
        snifferdataEntitiesTHL = snifferdataEntitiesTHL
                                .stream()
                                .filter(e -> {
                                    int halfFcs = Integer.parseInt(e.getFcs().substring(2, 4), 16);
                                    return halfFcs > 128;
                                })
                                .collect(Collectors.toList());

        HashMap<String, MaxAndMean> stringIntegerHashMap = new HashMap<>();
        for (SnifferdataEntity snifferdataEntity : snifferdataEntitiesTHL) {
            String originAddress = snifferdataEntity.getPayload().substring(12, 16);
            if(stringIntegerHashMap.containsKey(originAddress)){
                int hopCount = Integer.parseInt(snifferdataEntity.getPayload().substring(6, 8), 16);
                MaxAndMean maxAndMean = stringIntegerHashMap.get(originAddress);
                maxAndMean.setMax(Math.max(hopCount + 1, maxAndMean.getMax()));
                maxAndMean.setSize(maxAndMean.getSize() + 1);
                maxAndMean.setSum(maxAndMean.getSum() + hopCount);
                maxAndMean.setMeanValue((float)maxAndMean.getSum()/(float)maxAndMean.getSize() + 1);
            }else{
                int hopCount = Integer.parseInt(snifferdataEntity.getPayload().substring(6, 8), 16);
                MaxAndMean maxAndMean = new MaxAndMean();
                maxAndMean.setMax(Math.max(hopCount + 1, maxAndMean.getMax()));
                maxAndMean.setSize(maxAndMean.getSize() + 1);
                maxAndMean.setSum(maxAndMean.getSum() + hopCount);
                maxAndMean.setMeanValue((float)maxAndMean.getSum()/(float)maxAndMean.getSize() + 1);
                stringIntegerHashMap.put(originAddress, maxAndMean);
            }
        }
        if("0x0001".equals(srcAddress)){
            nodeInfo.setMaxHop("");
            nodeInfo.setMeanHop("");
        }else{
            nodeInfo.setMaxHop(String.valueOf(stringIntegerHashMap.get(srcAddress.substring(2)).getMax()));
            nodeInfo.setMeanHop(String.valueOf(stringIntegerHashMap.get(srcAddress.substring(2)).getMeanValue()));
        }
        /**
         * 包到达率
         */
        List<SnifferdataEntity> collect = snifferdataEntitiesTHL
                                            .stream()
                                            .filter(e -> e.getPayload().substring(12, 16).equals(srcAddress.substring(2)))
                                            .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                                            .collect(Collectors.toList());
        Set<String> set = new TreeSet<>();
        for (SnifferdataEntity snifferdataEntity : collect) {
            String payload = snifferdataEntity.getPayload();
            set.add(payload.substring(payload.length() - 8,payload.length() - 4));
        }
        List<SnifferdataEntity> snifferdataFromOrigin = this.snifferdataMapper.selectFromOrigin().stream()
                                                                .filter(e -> e.getPayload().substring(12, 16).equals(srcAddress.substring(2)))
                                                                .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                                                                .collect(Collectors.toList());
        if(!collect.isEmpty()){
            String payload = snifferdataFromOrigin.get(snifferdataFromOrigin.size() - 1).getPayload();
            System.out.println("set: " + set.size());
            int realPackageCount = Integer.parseInt(payload.substring(payload.length() - 8, payload.length() - 4),16);
            System.out.println("realPackageCount = " + realPackageCount);
            nodeInfo.setPackageDeliveryRate(String.format("%.2f",(float)set.size()/realPackageCount));
        }else{
            nodeInfo.setPackageDeliveryRate("0.00");
        }


        /**
         * 控制开销(广播包)
         */
        List<SnifferdataEntity> snifferdata = this.snifferdataMapper.selectByumberOfBroadCast(srcAddress);
        snifferdata = snifferdata.stream()
                .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                .collect(Collectors.toList());
        nodeInfo.setBroadcastPackage(String.valueOf(snifferdata.size()));

        /**
         * 路径扰动
         */
        List<SnifferdataEntity> snifferdataEntitiesBypathTurbulence = this.snifferdataMapper.selectBypathTurbulence(srcAddress);
        snifferdataEntitiesBypathTurbulence = snifferdataEntitiesBypathTurbulence.stream()
                .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                .collect(Collectors.toList());
        int pathTurb = 0;
        for (int i = 0; i <= snifferdataEntitiesBypathTurbulence.size() - 2; ++i) {
            if(!snifferdataEntitiesBypathTurbulence.get(i).getDestaddress().equals(snifferdataEntitiesBypathTurbulence.get(i + 1).getDestaddress())){
                pathTurb++;
            }
        }
        nodeInfo.setPathTurbulence(String.valueOf(pathTurb));

        /**
         * 流量
         * BroadcastPackageRate
         */
        List<SnifferdataEntity> snifferdataFlow = this.snifferdataMapper.selectByFlow(srcAddress);
        snifferdataFlow = snifferdataFlow.stream()
                .filter(e -> {
                    int halfFcs = Integer.parseInt(e.getFcs().substring(2, 4), 16);
                    return halfFcs > 128;
                })
                .collect(Collectors.toList());
        int flow = 0;
        for (SnifferdataEntity snifferdataEntity : snifferdataFlow) {
            flow += snifferdataEntity.getLength();
        }
        nodeInfo.setFlow(String.valueOf(flow));

        nodeInfo.setBroadcastPackageRate(String.format("%.2f",(float)snifferdata.size()/Integer.parseInt(nodeInfo.getNumberOfPackets())));
        /**
         * 副本数量
         * 副本率
         */
        if(srcAddress == "0x0001"){
            nodeInfo.setNumberOfCopies("0");
            nodeInfo.setNumberOfCopiesRate("0.00");

        }else{
            List<SnifferdataEntity> snifferdataBynumberOfCopies = this.snifferdataMapper.selectBynumberOfCopies(srcAddress);
            snifferdataBynumberOfCopies = snifferdataBynumberOfCopies.stream()
                    .filter(e -> Integer.parseInt(e.getFcs().substring(2, 4), 16) > 128)
                    .collect(Collectors.toList());
            HashSet<String> set2 = new HashSet<>();
            for (SnifferdataEntity snifferdataBynumberOfCopy : snifferdataBynumberOfCopies) {
                String originAddress = snifferdataBynumberOfCopy.getPayload().substring(12, 16);
                if(srcAddress.substring(2).equals(originAddress)){
                    String payload = snifferdataBynumberOfCopy.getPayload();
                    set2.add(payload.substring(6,8) + payload.substring(12, 20));
                }
            }
            nodeInfo.setNumberOfCopies(String.valueOf(set2.size()));
            nodeInfo.setNumberOfCopiesRate(String.format("%.2f",(float)set2.size()/Integer.parseInt(nodeInfo.getNumberOfPackets())));
        }

        System.out.println(nodeInfo);
        return nodeInfo;
    }
}