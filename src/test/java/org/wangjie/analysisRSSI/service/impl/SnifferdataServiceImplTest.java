package org.wangjie.analysisRSSI.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wangjie.analysisRSSI.mapper.SnifferdataMapper;
import org.wangjie.analysisRSSI.service.SnifferdataService;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class SnifferdataServiceImplTest {

    @Autowired
    private SnifferdataService snifferdataService;

    @Test
    public void calcNodeInfo() {
        snifferdataService.calcNodeInfo("0x0004");
    }

    @Test
    public void calcSystemInfo(){
        snifferdataService.calcSysetmIndicator(4);
    }
}